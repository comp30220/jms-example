# jms-example

Demonstrator of JMS with ActiveMQ that contains 2 projects.

*  receiver: implements a JMS receiver for a QUEUE
*  sender: implements a JMS sender for a QUEUE

To run this example, type:

'''docker-compose up'''