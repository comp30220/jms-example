package sender;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

public class Sender {
    public static void main(String[] args) {
        String host = "localhost";
        if (args.length > 0) {
            host = args[0];
        }
        System.out.println("Starting Sender on: " +host);

        try {
            ConnectionFactory connectionFactory =
                new ActiveMQConnectionFactory("failover://tcp://"+host+":61616");
            Connection connection = connectionFactory.createConnection();
            connection.setClientID("sender");
            Session session = connection.createSession(false,
                Session.AUTO_ACKNOWLEDGE);
            Queue queue = session.createQueue("TESTQUEUE");
            MessageProducer messageProducer = session.createProducer(queue);

            TextMessage textMessage = session.createTextMessage("Hello World");
            messageProducer.send(textMessage);
        } catch (JMSException e) { e.printStackTrace(); }
    }
}

